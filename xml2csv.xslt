<?xml version="1.0"?>
<!-- Copyright (c) 2016 Ilya Usvyatsky -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:csv="csv:csv" xmlns:my="http://schemas.microsoft.com/office/infopath/2003/myXSD" xmlns:xd="http://schemas.microsoft.com/office/infopath/2003" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
  <xsl:output method="text" encoding="utf-8" />
  <xsl:strip-space elements="*" />

  <xsl:variable name="delimiter" select="','" />

  <csv:columns>
    <column>Element</column>
    <column>Type</column>
    <column>Field</column>
  </csv:columns>

  <xsl:template match="xsd:schema">
    <!-- Output the CSV header -->
    <xsl:for-each select="document('')/*/csv:columns/*">
      <xsl:value-of select="."/>
      <xsl:if test="position() != last()">
        <xsl:value-of select="$delimiter"/>
      </xsl:if>
    </xsl:for-each>
    <xsl:text>&#xa;</xsl:text>
    <!-- Now process elements -->
    <xsl:for-each select="./xsd:element">
      <!-- Extract element name -->
      <xsl:variable name="elementName" select="./@name" />
      <!-- There are two classes of elements: scalars and sequences -->
      <xsl:choose>
        <xsl:when test="./xsd:complexType/xsd:sequence">
          <!-- This is a sequence -->
          <!-- Set element type -->
          <xsl:variable name="elementType" select="'xsd:complexType'" />
          <!-- For each field print elementName, elementType and fieldName -->
          <xsl:for-each select="./xsd:complexType/xsd:sequence/xsd:element">
            <xsl:variable name="fieldName" select="./@ref" />
            <xsl:value-of select="$elementName" />
            <xsl:value-of select="$delimiter" />
            <xsl:value-of select="$elementType" />
            <xsl:value-of select="$delimiter" />
            <xsl:value-of select="$fieldName" />
            <xsl:text>&#xa;</xsl:text>
          </xsl:for-each>
        </xsl:when>
        <xsl:otherwise>
          <!-- This is a scalar -->
          <!-- Get element type -->
          <xsl:variable name="elementType" select="@type" />
          <!-- Print elementName and elementType -->
          <xsl:value-of select="$elementName" />
          <xsl:value-of select="$delimiter" />
          <xsl:value-of select="$elementType" />
          <xsl:value-of select="$delimiter" />
          <xsl:text>&#xa;</xsl:text>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:for-each>
  </xsl:template>
</xsl:stylesheet>
